﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.Services;
using DeclarationIncident.ViewModels;
using DeclarationIncident.ViewModels.Convert;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeclarationIncident.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IncidentTypePage : ContentPage
	{
        IncidentTypeViewModel viewModel = new IncidentTypeViewModel();
        
        public IncidentTypePage()
        {
            InitializeComponent();

            viewModel = new IncidentTypeViewModel();
            ListViewIncidentType.ItemsSource = viewModel.GetListIncidentTypes();
        }

        private async void ListViewIncidentType_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var obj = (IncidentTypeConv) e.SelectedItem;
            var idIncidentType = obj.idIncidentType.ToString();
            
            if (idIncidentType != null)
            {
                Settings.CurrentCurrentIncidentType_Id = idIncidentType;
                await Navigation.PushAsync(new IncidentPage());
            }
        }
    }
}