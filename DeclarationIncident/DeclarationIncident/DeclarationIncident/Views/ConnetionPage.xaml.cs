﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.Services;
using DeclarationIncident.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeclarationIncident.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConnetionPage : ContentPage
	{
       // Settings settings = new Settings();
        UserViewModel model = new UserViewModel();

		public ConnetionPage ()
		{
			InitializeComponent ();
		}

        private async void OnButtonConnexionClicked(object sender, EventArgs e)
        {
            string email = emailEntry.Text;
            string password = passwordEntry.Text;

            var userAuthenticated = model.GetAuthenticatedUser(email, password);

            if (userAuthenticated != null)
            {
                /*
                 * Enregistrement de des infos de l'utilisateur courant
                 */
                Settings.currentUser = userAuthenticated.lastName + " " + userAuthenticated.firstName;
                Settings.currentUserId = userAuthenticated.idUser.ToString();

                await Navigation.PushAsync(new LocationPage());
            }
            else
            {
                var labelErreur = new Label();
                labelErreur.Text = "Email ou mot de passe incorrecte !";
                labelErreur.IsVisible = true;

                await ShowMessage(" Mot de passe ou email incorrect 2 ! ", "Prompt", "Ok", async () =>
                {
                    await ShowMessage("OK was pressed", "Message", "OK", null);
                });
            }

        }

        public async Task ShowMessage(string message,
            string title,
            string buttonText,
            Action afterHideCallback)
        {
            await DisplayAlert(
                title,
                message,
                buttonText);

            afterHideCallback?.Invoke();
        }
    }
}