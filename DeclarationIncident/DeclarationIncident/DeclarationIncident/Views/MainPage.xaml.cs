﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.ViewModels;
using DeclarationIncident.Views;
using Xamarin.Forms;

namespace DeclarationIncident
{
    public partial class MainPage 
    {
        public MainPage()
        {            
            InitializeComponent();
        }

        /*
         *Nouveau utilisateur
         */
        private async void OnButtonRegisterAccountClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegisterAccountPage());
        }

        private async void OnButtonConnexionClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ConnetionPage());
        }
    }
}
