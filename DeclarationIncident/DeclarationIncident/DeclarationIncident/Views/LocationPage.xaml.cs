﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.Services;
using DeclarationIncident.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeclarationIncident.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LocationPage : ContentPage
	{
        LocationViewModel model;


        public LocationPage ()
		{
			InitializeComponent ();

            model = new LocationViewModel();
            LocationView.ItemsSource = model.GetLocationList();
        }

        private async void LocationView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                var obj = (Location) e.SelectedItem;
                //var idLocation = Convert.ToInt32(obj.idLocation);
                var idLocation = obj.idLocation.ToString();

                if (idLocation !=null)
                {
                    //Navigate to view model with parameters
                    Settings.Currentlocation_Id = idLocation;
                    await Navigation.PushAsync(new IncidentTypePage());
                }
                    

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}