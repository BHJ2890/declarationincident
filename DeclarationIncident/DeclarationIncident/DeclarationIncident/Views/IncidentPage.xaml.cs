﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.Services;
using DeclarationIncident.ViewModels;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeclarationIncident.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IncidentPage : ContentPage
    {
        IncidentViewModel model = new IncidentViewModel();
        private MediaFile mediaFile;


        public IncidentPage()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

         async void OnValidateButtonClicked(object sender, EventArgs e)
         {
            /*
             * Enregistre l'image et recupere
             * le nom
             */
            var fileName = await UploadFile();

            /*
             * Enregistre la declaration d'incident
             */
            var description = Libelle.Text;
            var result = model.DeclareIncidentAsync(description, fileName);
         }


         private async void OnTakePhotoButtonClicked(object sender, EventArgs e)
         {
             await CrossMedia.Current.Initialize();
             
             if (!CrossMedia.Current.IsCameraAvailable 
                 || !CrossMedia.Current.IsTakePhotoSupported)
             {
                 await DisplayAlert("Camera non disponible !",":(Camera non disponible.","Ok");
                 return;
             }

             try
             {
                 mediaFile = await CrossMedia.Current.TakePhotoAsync(
                     new StoreCameraMediaOptions()
                     {
                         Directory = "Pictures",
                         Name = DateTime.Now + "MyImage.jpg",
                        SaveToAlbum = true
                     });
             }
             catch (Exception exception)
             {
                 Console.WriteLine(exception);
                 throw;
             }

             if(mediaFile == null)
                 return;

            DeclaredIncidentImage.Source = ImageSource.FromStream(()=>
            {
                return mediaFile.GetStream();
                //var stream = mediaFile.GetStream();
                //mediaFile.Dispose();
                //return stream;
            });
         }

        private async Task<string> UploadFile()
        {
            var content = new MultipartFormDataContent();

            content.Add(new StreamContent(mediaFile.GetStream()),
                "\"file\"",
                $"\"{mediaFile.Path}\"");

   
                var httpClient = new HttpClient();
                var upLoadServiceBaseAdress = "http://127.0.0.1:61121/api/Uploads/";

                var httpMessage = await httpClient.PostAsync(upLoadServiceBaseAdress, content);
                var result = await httpMessage.Content.ReadAsStringAsync();

                /*
                 * Retourne le nom de l'image
                 */
                return result;
        }
  
    }
}