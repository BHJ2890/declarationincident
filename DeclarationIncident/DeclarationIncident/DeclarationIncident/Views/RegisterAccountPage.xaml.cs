﻿using DeclarationIncident.Models.POCO;
using DeclarationIncident.Services;
using DeclarationIncident.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DeclarationIncident.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterAccountPage : ContentPage
    {
        UserViewModel userVm = new UserViewModel();

		public RegisterAccountPage ()
		{
			InitializeComponent();
		}
        
        public async void Button_Clicked(object sender, EventArgs e)
        {
            var newUser = new User()
            {
                lastName = lastName.Text,
                firstName = firstName.Text,
                email = emailEntry.Text,
                password = passwordEntry.Text,
                idRole = 1
            };

            var result = await userVm.RegisterNewUser(newUser);
        }
    }
}