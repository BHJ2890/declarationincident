﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class Role
    {
        public int idRole { get; set; }
        public string libelle { get; set; }

        public virtual List<User> user { get; set; }
    }
}