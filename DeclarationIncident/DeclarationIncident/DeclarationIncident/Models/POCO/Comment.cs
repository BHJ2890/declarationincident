﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class Comment
    {
        public int idIncidentMention { get; set; }
        public string Libelle { get; set; }
        public int idIncident { get; set; }
        public int idUser { get; set; }

        public virtual Incident incident { get; set; }
        public virtual User user { get; set; }
    }
}