﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class Notification
    {
        public int idNotification { get; set; }
        public string Description { get; set; }
        public int idIncident { get; set; }
       
        public virtual Incident Incident { get; set; }
    }
}