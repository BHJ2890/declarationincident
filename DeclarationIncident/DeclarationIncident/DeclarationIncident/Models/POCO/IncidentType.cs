﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class IncidentType
    {
        public int idIncidentType { get; set; }
        public string Libelle { get; set; }
        public string imageTitle { get; set; }
        public string imagePath { get; set; }
    }
}