﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class ToDoTask
    {
        public int IdToDoTask { get; set; }
        public string nameTask { get; set; }
        public DateTime dateTask { get; set; }
        public int idIncident { get; set; }
        public int idUser { get; set; }
       
        public virtual Incident incident { get; set; }
        public virtual User user { get; set; }
    }
}