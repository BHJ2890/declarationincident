﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class User
    {
        public int idUser { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public int idRole { get; set; }

        public virtual Role role { get; set; }
        public virtual List<Incident> Incident { get; set; }
        public virtual List<Comment> comment { get; set; }
    }
}