﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class Location
    {
        public int idLocation { get; set; }
        public string nameLocation { get; set; }

        public virtual List<Incident> incident { get; set; }
    }
}