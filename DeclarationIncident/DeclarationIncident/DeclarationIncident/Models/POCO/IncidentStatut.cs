﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeclarationIncident.Models.POCO
{
    public class IncidentStatut
    {
        public int IdIncidentStatut { get; set; }
        public string description { get; set; }

        public virtual List<Incident> incident { get; set; }
    }
}