﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;


namespace DeclarationIncident.Models.RestClients
{
    public class RestClient<T>
    {
        HttpClient httpClient = new HttpClient();
        /*
         *
         */
        public async Task<List<T>> GetAsync(string url)
        {
            try
            {
                var uri = new Uri(string.Format(url, string.Empty));
                var response = await httpClient.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<T>>(content);
                }

               return null;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
         *
         */
        public async Task<bool> PostAsync(T user,string url)
        {
            try
            {
                var jsonRes = JsonConvert.SerializeObject(user);
                var content = new StringContent(jsonRes, Encoding.UTF8, "application/json");

                var result = await httpClient.PostAsync(url, content);

                return result.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
        * Create page repo
        */
        public async Task<string> PostAsyncImage(MultipartFormDataContent content)
        {
            try
            {
                var upLoadServiceBaseAdress = "http://127.0.0.1:61121/api/Uploads/";

                var httpMessage = await httpClient.PostAsync(upLoadServiceBaseAdress, content);

                if (httpMessage.IsSuccessStatusCode)
                {
                    var result = await httpMessage.Content.ReadAsStringAsync();
                    return result;
                }

                var message = "Erreur " + httpMessage.ReasonPhrase;
                return null;
            }
            catch (Exception ex)
            {
               return ex.Message;
            }
        }

        /*
         * Get authenticated user
         */
        public async Task<User> GetAuthenticatedUser(string email, string password)
        {
            var url = string.Format("http://127.0.0.1:61121/api/Authentication/?password={0}&email={1}", password, email);
            try
            {
                var response = await httpClient.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<User>(content);
                }

                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<Byte[]> GetFile(string fileName)
        {

            var completeUrl = string.Format("http://127.0.0.1:61121/api/Uploads/?fileName={0}", fileName);

            using (var httpClients = new HttpClient())
            {
                try
                {
                    httpClients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("image/jpg"));
                    var response = await httpClients.GetAsync(completeUrl);

                    return await response.Content.ReadAsByteArrayAsync();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

        }
    }
}
