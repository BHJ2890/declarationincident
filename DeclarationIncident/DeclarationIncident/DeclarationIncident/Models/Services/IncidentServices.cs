﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.RestClients;

namespace DeclarationIncident.Models.Services
{
    class IncidentServices
    {
        RestClient<Incident> restClient = new RestClient<Incident>();
        private const string url = "http://127.0.0.1:61121/api/Incident/";

        public async Task<bool> DeclareIncidentAsync(Incident incident)
        {
            try
            {
                var result = await restClient.PostAsync(incident, url);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public string GetImageName(MultipartFormDataContent content)
        {
            var FileName = restClient.PostAsyncImage(content).Result;
            return FileName;
        }

       
    }
}
