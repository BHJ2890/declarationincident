﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.RestClients;
using Plugin.Media.Abstractions;

namespace DeclarationIncident.Models.Services
{
    class LocationServices
    {
        RestClient<POCO.Location> restClient = new RestClient<POCO.Location>();

      private const string getUrl = "http://127.0.0.1:61121/api/Location/";
        
        /*
         * Get a list of locations
         */
        public List<POCO.Location> GetLocations()
        {
            try
            {
                var listTypeIncident = Task.Run( async () => await restClient.GetAsync(getUrl)).Result;  
                return listTypeIncident;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

    }
}
