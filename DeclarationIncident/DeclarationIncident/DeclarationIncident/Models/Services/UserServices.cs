﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Collections.ObjectModel;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.ComponentModel;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.RestClients;
using DeclarationIncident.Models.Services;
using Xamarin.Forms;

namespace DeclarationIncident.Services
{
    class UserServices
    {
        private const string url = "http://10.0.2.2:61121/api/user/";
        private RestClient<User> restClient = new RestClient<User>();
        //
        public async Task<bool> registerUser(User user)
        {
            var result = restClient.PostAsync(user, url);
            return await result;
        }

        public async Task<User> GetAuthenticatedUser(string email, string password)
        {
            var user = await restClient.GetAuthenticatedUser(email, password);

            if (user != null)
            {
                Application.Current.Properties["UserAuthenticatedInfo"] = user;
                Settings.currentUser = user.firstName +" "+user.firstName;
            }
            return user;
        }
    }
}
