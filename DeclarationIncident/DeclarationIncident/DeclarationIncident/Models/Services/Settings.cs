﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeclarationIncident.Models.Services
{
    public class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string userName = "userName_key";
        private const string userId = "user_Id";
        private const string CurrentIncidentType_Id = "incident_Id";
        private const string location_Id = "location_Id";
        private static readonly string SettingsDefault = string.Empty;

        #endregion

        public static string Currentlocation_Id
        {
            get
            {
                return AppSettings.GetValueOrDefault(location_Id, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(location_Id, value);
            }
        }

        public static string CurrentCurrentIncidentType_Id
        {
            get
            {
                return AppSettings.GetValueOrDefault(CurrentIncidentType_Id, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CurrentIncidentType_Id, value);
            }
        }

        public static string currentUser
        {
            get
            {
                return AppSettings.GetValueOrDefault(userName, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(userName, value);
            }
        }

        public static string currentUserId
        {
            get
            {
                return AppSettings.GetValueOrDefault(userId, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(userId, value);
            }
        }

    }
}
