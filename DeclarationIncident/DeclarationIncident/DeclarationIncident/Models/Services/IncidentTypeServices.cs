﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.RestClients;

namespace DeclarationIncident.Models.Services
{
    class IncidentTypeServices
    {
        private const string url = "http://127.0.0.1:61121/api/IncidentType/";
        private RestClient<IncidentType> restClient = new RestClient<IncidentType>();

        /*
         *
         */
        public List<IncidentType> GetIncidentType()
        {
           var listTypeIncident = Task.Run(
                async () => await restClient.GetAsync(url)).Result;

            return listTypeIncident;
        }

        /*
         *
         */
        public Byte[] GetFile(string fileName)
        {
            var image = Task.Run(
            async () => await restClient.GetFile(fileName)).Result;

            return image;
        }
    }
}
