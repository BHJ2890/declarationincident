﻿using DeclarationIncident.Models.POCO;
using DeclarationIncident.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using DeclarationIncident.Models.Services;
using DeclarationIncident.ViewModels.Convert;
using Xamarin.Forms;
using System;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace DeclarationIncident.ViewModels
{
    public class IncidentTypeViewModel : INotifyPropertyChanged
    {
        private IncidentTypeServices services = new IncidentTypeServices();
        //public ObservableCollection <IncidentType> listIncitype = new ObservableCollection<IncidentType>();

        public ObservableCollection<IncidentTypeConv> listIncitype = new ObservableCollection<IncidentTypeConv>();
        public string essaie = "Je suis fatigué";
        public int incidentId { get; set; }

        public IncidentTypeViewModel(int id)
        {
           incidentId = id;
        }
        public IncidentTypeViewModel()
        {
            
        }

        public ObservableCollection<IncidentTypeConv> GetListIncidentTypes()
        {
            var list = services.GetIncidentType();
           
            foreach (var item in list)
            {
                listIncitype.Add(
                    new IncidentTypeConv()
                    {
                        idIncidentType = item.idIncidentType,
                        Libelle = item.Libelle,
                        images = BytesToImage(services.GetFile(item.imageTitle))
                    });
            }
            //listIncitype = new ObservableCollection<IncidentType>(list);

            return listIncitype;
        }
    
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //Convert a byte array into an image
        public Image BytesToImage(byte[] value)
        {
            Image image = new Image();
            if (value != null)
            {
                Stream stream = new MemoryStream(value);
                image.Source = ImageSource.FromStream(() => { return stream; });
            }
            return image;
        }
 
    }

}
