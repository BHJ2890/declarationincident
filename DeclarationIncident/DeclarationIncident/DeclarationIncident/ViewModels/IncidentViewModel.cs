﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.Services;
using Plugin.Media.Abstractions;

namespace DeclarationIncident.ViewModels
{
    class IncidentViewModel : INotifyPropertyChanged
    {
        IncidentServices incidentServices = new IncidentServices();
        
        /*
         *
         */
        public bool DeclareIncidentAsync(string description, string fileName)
        {
           var incident = new Incident()
            {
                Description = description,
                dateIncident = DateTime.Now.ToString("yyyyMMdd"),
                imageTitle = fileName,
                IdIncidentStatut = 1,
                idUser =System.Convert.ToInt32(Settings.currentUserId),
                idLocation = System.Convert.ToInt32(Settings.Currentlocation_Id),
                idIncidentType = System.Convert.ToInt32(Settings.CurrentCurrentIncidentType_Id)
            };

            var x = incident;
            var result = Task.Run(async () => await incidentServices.DeclareIncidentAsync(incident)).Result;
            return result;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
