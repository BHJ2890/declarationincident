﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DeclarationIncident.ViewModels.Convert
{
    public class IncidentTypeConv
    {
        public int idIncidentType { get; set; }
        public string Libelle { get; set; }
        public Image images { get; set; }
    }
}

