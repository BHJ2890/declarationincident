﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Models.Services;

namespace DeclarationIncident.ViewModels
{
    public class LocationViewModel
    { 
        public ObservableCollection<Location> locationList;

        LocationServices locationServices  = new LocationServices();

        /*
         *
         */
        public LocationViewModel()
        {
            
        }

        /*
         * Obtient la liste des lieux
         */
        public ObservableCollection<Location> GetLocationList()
        {
            var listLocation = new ObservableCollection<Location>(locationServices.GetLocations()); 
            return listLocation;
        }
    }
}
