﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using DeclarationIncident.Models.POCO;
using DeclarationIncident.Services;

namespace DeclarationIncident.ViewModels
{
    class UserViewModel : INotifyPropertyChanged
    {
        UserServices userServices = new UserServices();
        //
        public async Task<bool> RegisterNewUser(User user)
        {
            var result = await userServices.registerUser(user);
            return result;
        }

        public User GetAuthenticatedUser(string email, string password)
        {
            var user = Task.Run( async () => await userServices.GetAuthenticatedUser(email, password)).Result; 
            return user;
        }

        //Initilalisation de la liste des user

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
