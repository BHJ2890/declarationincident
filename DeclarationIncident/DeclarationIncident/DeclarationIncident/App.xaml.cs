﻿using System;
using DeclarationIncident.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DeclarationIncident
{
    public partial class App : Application
    {
        public App()
        {
            //System.Security.Cryptography.AesCryptoServiceProvider AES = new System.Security.Cryptography.AesCryptoServiceProvider();
            //System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;

            InitializeComponent();

            //MainPage = new DeclarationIncident.Views.RegisterAccountPage();
             MainPage = new DeclarationIncident.Views.IncidentPage();
          //  MainPage = new NavigationPage(new MainPage());
           // MainPage = new DeclarationIncident.Views.IncidentTypePage();
          // MainPage = new DeclarationIncident.Views.LocationPage();

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
