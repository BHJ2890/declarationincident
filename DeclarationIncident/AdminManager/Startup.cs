﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdminManager.Startup))]
namespace AdminManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
