﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;

namespace AdminManager.Controllers
{
    //[Authorize]
    public class LocationController : Controller
    {
        // GET: Location
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> ListAsync()
        {
            var items = new LocationServices();
            var listedLocations =await items.GetLocationAsync();
        
            return Json(listedLocations, JsonRequestBehavior.AllowGet);
        }

        // GET: Location
        [HttpPost]
        public async Task<ActionResult> CreateLocation(Location location)
        {
            if (ModelState.IsValid)
            {
                var locationServices = new LocationServices();

                //
                var task = locationServices.CreateLocationAsync(location);
                await task;
                if (task.Exception !=null)
                    ModelState.AddModelError("","Unable to add the asset ");
                return View("Index");
            }

            return null;
        }
    }
}