﻿using AdminManager.Models.CustomAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using AdminManager.Models.ViewModel;
using Newtonsoft.Json;

namespace AdminManager.Controllers
{
    //[Authorize(Roles = "Admin")]
    //[Authorize(Roles = "Admin")]
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Account(string ReturnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return LogOut();
            }

            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(string email, string password, string grant_type, string ReturnUrl = "")
        {
            if (ModelState.IsValid)
            {
                     //var user = (CustomMemberShipUser)Membership.GetUser(loginView.email, false);
                    var services = new AdminServices();
                    var user = await  services.ValidateUser(email, password);
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(user.idUser.ToString(), false);

                        if (!string.IsNullOrWhiteSpace(ReturnUrl) && Url.IsLocalUrl(ReturnUrl))
                            return Redirect(ReturnUrl);

                    //return RedirectToAction("/Home/Index");
                    return RedirectToAction("Index", "Home");
                    }

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                    return RedirectToAction("Index","Home");
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
            }
            ModelState.AddModelError("", "Something Wrong : Username or Password invalid ^_^ ");
            return View();
        }

        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("Cookie1", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Index", null);
        }

    }
}