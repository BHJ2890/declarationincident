﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using AdminManager.Models.ViewModel;

namespace AdminManager.Controllers
{
    public class ToDoTaskController : Controller
    {
        ToDoTaskVM model = new ToDoTaskVM();
        ToDoTaskServices toDoTaskServices = new ToDoTaskServices();

        // GET: ToDoTask
        [HttpGet]
        public async Task<ActionResult> Create(string incidentDescription, string incidentLocation, int id = 0)
        {
            if (id > 0)
            {
                var adminServices = new AdminServices();
                model = new ToDoTaskVM()
                {
                    UsersList = await adminServices.GetAllUserAsync(), //List de tout les utilisateurs
                    idIncident = id,
                    incidentDescription = incidentDescription,
                    incidentLocation = incidentLocation
                };
                return PartialView(model);
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(ToDoTask model)
        {
            if (ModelState.IsValid)
            {
                model.dateTask = DateTime.Now;
                var createTaskToDo =await toDoTaskServices.CreateToDoTaskServices(model);

                if (createTaskToDo)
                    return RedirectToAction("Index", "Incident");
                return HttpNotFound();
            }
            return View();
        }

    }
}