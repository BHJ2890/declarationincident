﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using AdminManager.Models.ViewModel;

namespace AdminManager.Controllers
{
    //[Authorize]
    public class IncidentTypeController : Controller
    {
        IncidentTypeServices services = new IncidentTypeServices();
        static HttpClient client = new HttpClient();

        // GET: IncidentType
        public async Task<ActionResult> Index()
        {
            var listInc = new List<IncidentTypeVM>();
            var listItem = await services.GetAllIncidentTypeAsync();

            foreach (var items in listItem)
            {
                listInc.Add(
                    new IncidentTypeVM()
                    {
                        Libelle = items.Libelle,
                        imageTitle = items.imageTitle,
                        imagePath = items.imagePath,
                        idIncidentType = items.idIncidentType
                    });
                }

            return View(listInc);
        }

        [HttpGet]
        public ActionResult CreateIncidentType()
        {
            return View();
        }

       [HttpPost]
        public async Task<ActionResult> CreateIncidentType(IncidentType model)
        {
            //byte[] bytes;
            //using (BinaryReader br = new BinaryReader(imageFile.InputStream))
            //{
            //    bytes = br.ReadBytes(imageFile.ContentLength);
            //}

            ////
            //IncidentType entities = new IncidentType()
            //{
            //    imagePath = Path.GetFileName(imageFile.FileName),
            //    Libelle = model.Libelle,
            //    imageTitle = imageFile.ContentType,
            //    fileContent = bytes,
            //    idUser = 1
            //};
            
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                
                var content = new MultipartFormDataContent();

                byte[] fileBytes = new byte[file.InputStream.Length + 1];
                file.InputStream.Read(fileBytes, 0, fileBytes.Length);

                var fileContent = new ByteArrayContent(fileBytes);
               fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };


                //content.Add(new StreamContent(fileContent,
                //    "\"file\"",
                //    $"\"{fileContent.p.Path}\"");

                content.Add(fileContent);

                model.imageTitle = file.FileName;
                var result = await services.CreateIncidentType(model, content);

                //
                if (result)
                    return RedirectToAction("Index");
                return View(model);
            }

            return null;

            //var content = new MultipartFormDataContent();

            //content.Add(new StreamContent(mediaFile.gets),
            //    "\"file\"",
            //    $"\"{mediaFile.Path}\"");


        }

        public async Task<ActionResult> DeleteIncidentType(int id)
        {
            if (id > 0)
            {
                var result = await services.DeleteById(id);
                if (result)
                    return RedirectToAction("Index");
            }
            return View();
        }
    }
}