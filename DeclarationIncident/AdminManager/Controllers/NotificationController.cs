﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using AdminManager.Models.ViewModel;

namespace AdminManager.Controllers
{
    public class NotificationController : Controller
    {
      NotificationServices notificationServices = new NotificationServices();

        // GET: Notification
            public ActionResult _Create(int id)
            {
                if (id > 0)
                {
                    var model = new NotificationVM()
                    {
                        idIncident = id,
                    };
            
                    return PartialView(model);
                }
                return null;
            }

            [HttpPost]
            public ActionResult Create(Notification model)
            {
                if (ModelState.IsValid)
                {
                    var result = notificationServices.CreateNotification(model);
                    return RedirectToAction("Index","Incident");
                }
                return RedirectToAction("Index", "Incident");
            }

            public ActionResult Notification(int id = 0)
            {
                var model = new List<Notification>();
                if (id > 0)
                {
                    model = notificationServices.GetAllNotification().Where(u => u.idIncident == id).ToList();
                }
                
                return PartialView("_Notification", model);
            }
    }
}
