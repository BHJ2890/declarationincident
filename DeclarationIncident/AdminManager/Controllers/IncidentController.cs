﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using AdminManager.Models.ViewModel;

namespace AdminManager.Controllers
{
    //[Authorize]
    public class IncidentController : Controller
    {
        IncidentServices incidentServices = new IncidentServices();
        IncidentVM incidentVm = new IncidentVM();


        // Affiche la vue principale
        public ActionResult Index()
        {
            return View();
        }

        /*
         * Affiche la vue partielle
         * toute les 10 secondes un appel
         * ajax effectue une mis à jour périodique
         */
        public async Task<ActionResult> _GetIncidentList()
        {
            var result = await incidentServices.GetIncidentAsync();
            var model = new List<IncidentVM>();

            foreach (var items in result)
            {
                model.Add(
                    new IncidentVM()
                    {
                        idIncident = items.idIncident,
                        Description = items.Description,
                        dateIncident = items.dateIncident,
                        incidentType = items.incidentType,
                        Location = items.Location,
                        incidentStatut = items.incidentState,
                        IdIncidentState = items.IdIncidentStatut,
                        //statuName = items.incidentState.description,
                        user = items.user
                    });
            }

            if (model != null)
                return PartialView("_GetIncidentList",model);
          
            return HttpNotFound();
        }

        [HttpGet]
        public async Task<ActionResult> Detail(int id = 0)
        {
            if (id > 0)
            {
                var userInfo =await incidentServices.GetEntityById(id);

                if (userInfo != null)
                {
                    var details = new IncidentVM()
                    {
                        idIncident = userInfo.idIncident,
                        Description = userInfo.Description,
                        dateIncident = userInfo.dateIncident,
                        incidentStatut = userInfo.incidentState,
                        imageTitle = userInfo.imageTitle
                    };
                    return View(details);
                }
                    return null;
            }

            return View("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id = 0)
        {
            if (id > 0)
            {
                var deleteIncident =await incidentServices.DeleteIncident(id);

                if (deleteIncident == true)
                    return RedirectToAction("Index");
                return HttpNotFound();
            }
            return View("Index");
        }

        /*
         * Affiche le statut de l'insident
         */
        public async Task<ActionResult> ToDoTaskStatStatut(int id)
        {
            //
            if (id > 0)
            {
                var toDoTaskStateServices = new ToDoTaskStateServices();

                var model = await toDoTaskStateServices.GetEntityById(id);
             
                if (model != null)
                    return PartialView("_TaskStatut",model);

                return PartialView("_TaskStatut", model.description = "En attente" );
            }
            return RedirectToAction("Index");
        }

        
        /*
         *
         */
        //public ActionResult _CreateNotification(int id)
        //{
        //    var notificationType = new NotificationTypeServices();

        //    if (id > 0)
        //    {
        //        var model = new NotificationVM()
        //        {
        //            idIncident = id,
        //            ListNotificationTypes = notificationType.GetAllNotificationType()
        //        };
        //        var mod = model;
        //        return PartialView(model);
        //    }

        //    return null;
        //}
    }
}