﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.Services;

namespace AdminManager.Controllers
{
    public class ToDoTaskStateController : Controller
    {
        ToDoTaskStateServices toDoTaskStateServices = new ToDoTaskStateServices();

        // GET: ToDoTaskState
        public async Task<ActionResult> Index(int id)
        {
            if (id > 0)
            {
                var model = await toDoTaskStateServices.GetEntityById(id);

                if (model != null)
                    return PartialView(model);
                return null;
            }

            return null;
        }
    }
}