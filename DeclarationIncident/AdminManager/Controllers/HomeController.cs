﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using AdminManager.Models.ViewModel;

namespace AdminManager.Controllers
{
    public class HomeController : Controller
    {
        AdminServices adminServices = new AdminServices();

        public async Task<ActionResult> Index()
        {
            var listUser =  adminServices.GetAllUserAsync();
            return View(await listUser);
        }

      public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Create()
        {
            var categoryServices = new Models.Services.RoleServices();
            UserVM ViewModel = new UserVM()
            {
                roles = await categoryServices.GetAllCategoryAsync()
            };

            return PartialView("_Create", ViewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                var result = adminServices.CreateUserAsinc(user);
                if (result == true)
                    return RedirectToAction("Index");
                return View();
            }
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> DropDownListCategory()
        {
           //
               var categoryServices = new Models.Services.RoleServices();
                RoleVM ViewModel = new RoleVM()
                {
                    categories = await categoryServices.GetAllCategoryAsync()
                };

            return PartialView("_DropDownListCategory", ViewModel);
        }
    }
}