﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;

namespace AdminManager.Controllers
{
    //[Authorize]
    public class UserCategoryController : Controller
    {
        private RoleServices roleServices = new Models.Services.RoleServices();

        [HttpGet]
        public async Task<ActionResult> Index()
        {
           var ListCategory = await roleServices.GetAllCategoryAsync();

            if(ListCategory !=null )
                return View( ListCategory);
            return HttpNotFound();
        }

        public ActionResult _CreateCategory()
        {
            return PartialView("_CreateCategory");
        }

        [HttpPost]
        public async Task<ActionResult> Create(Models.POCO.Role model)
        {
            if (ModelState.IsValid)
            {
                var result = await roleServices.CreateCategoryAsinc(model);

                if (result)
                {
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> DeleteCategory(int id = 0)
        {
            if (id > 0)
            {
                var isDelete =await roleServices.DeleteCategory(id);

                if (isDelete)
                    return RedirectToAction("Index");
                return HttpNotFound();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> EditCategory(int id = 0)
        {
            if (id > 0)
            {
                var getCategory = await roleServices.GetEntityById(id);
                return View("_CreateCategory", getCategory);
            }

            return RedirectToAction("Index");
        }
    }
}