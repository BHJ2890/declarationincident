﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminManager.Models.POCO
{
    public class Comment
    {
        public int idIncidentMention { get; set; }
        [Required]
        public string Libelle { get; set; }
        [Display(Name = "Incident")]
        public int idIncident { get; set; }
        [Display(Name = "Utilisateur")]
        public int idUser { get; set; }

        public virtual Incident incident { get; set; }
        [ForeignKey("idUser")]
        public virtual User user { get; set; }
    }
}