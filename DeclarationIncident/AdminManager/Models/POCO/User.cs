﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Web;

namespace AdminManager.Models.POCO
{
    public class User
    {
        public int idUser { get; set; }
        [Display(Name = "Prénom ")]
        public string firstName { get; set; }
        [Display(Name = "Nom "), Required]
        public string lastName { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Saisi obligatoire")]
        [CompareAttribute("password", ErrorMessage = "Mot de pas incorrect ")]
        public string confirmPassWord { get; set; }

        [Display(Name = "Mot de passe"), Required, DataType(dataType:DataType.Password)]
        public string password { get; set; }
        [Display(Name = "Email"), Required]
        public string email { get; set; }
        [Display(Name = "Catégorie"), Required]
        public int idRole { get; set; }

        public virtual Role role { get; set; }
        public virtual List<Incident> Incident { get; set; }
        public virtual List<Comment> comment { get; set; }
    }
}