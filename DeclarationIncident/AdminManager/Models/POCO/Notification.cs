﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminManager.Models.POCO
{
    public class Notification
    {
        public int idNotification { get; set; }
        public string Description { get; set; }
        public int idIncident { get; set; }
       
        public virtual Incident Incident { get; set; }
    }
}