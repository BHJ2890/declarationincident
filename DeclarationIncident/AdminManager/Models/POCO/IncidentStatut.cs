﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminManager.Models.POCO
{
    public class IncidentStatut
    {
        public int IdIncidentStatut { get; set; }
        [Display(Name = "Statut")]
        public string description { get; set; }

        public virtual List<Incident> incident { get; set; }
    }
}