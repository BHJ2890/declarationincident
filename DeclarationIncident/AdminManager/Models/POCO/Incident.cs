﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminManager.Models.POCO
{
    public class Incident
    {
        public int idIncident { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Titre")]
        public string imageTitle { get; set; }
        [Display(Name = "Chemin d'acces")]
        public string imagePath { get; set; }
        [Display(Name = "Date")]
        public string dateIncident { get; set; }

        [Display(Name = "Lieu")]
        public int idLocation { get; set; }
        [Display(Name = "Type d'incident")]
        public int idIncidentType { get; set; }
        [Display(Name = "Utilisateur")]
        public int idUser { get; set; }
        [Display(Name = "Statut ")]
        public int IdIncidentStatut { get; set; }

        public virtual Location Location { get; set; }
        public virtual User user { get; set; }
        public virtual IncidentType incidentType { get; set; }
        public virtual IncidentStatut incidentState { get; set; }

        public virtual List<Comment> comment { get; set; }
        public virtual List<Notification> notification { get; set; }

    }
}