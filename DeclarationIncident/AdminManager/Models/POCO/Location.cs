﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminManager.Models.POCO
{
    public class Location
    {
        public int idLocation { get; set; }
        [Display(Name = "Lieu ")]
        public string nameLocation { get; set; }

      //  public virtual List<Incident> incident { get; set; }
    }
}