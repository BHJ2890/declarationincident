﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminManager.Models.POCO
{
    public class IncidentType
    {
        public int idIncidentType { get; set; }
        public string Libelle { get; set; }
        [Display(Name = "Titre"),Required]
        public string imageTitle { get; set; }
        [Display(Name = "Lien"),Required]
        public string imagePath { get; set; }
    }
}