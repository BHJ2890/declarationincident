﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminManager.Models.POCO
{
    public class Information
    {
        public int idInformation { get; set; }
        [Display(Name = "Text ")]
        public string texteInformation { get; set; }
    }
}