﻿using AdminManager.Models.POCO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace AdminManager.Models.RestClients
{
    public class RestClient<T>
    {

        HttpClient httpClient = new HttpClient();

        /*
         *
         */
        public async Task<T> GetEntityById(string url)
        {
            try
            {
                var jsonResponse = await httpClient.GetStringAsync(url);

                var taskModel = JsonConvert.DeserializeObject<T>(jsonResponse);
                return taskModel;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
         *Http post
         */
        public async Task<List<T>> GetAsync(string url)
        {
            try
            {
               var jsonResponse = await httpClient.GetStringAsync(url);

                var taskModel = JsonConvert.DeserializeObject<List<T>>(jsonResponse);
                return taskModel;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
         *Http Post 
         */
        public async Task<bool> PostAsync(T user,string url)
        {
            try
            {
                var jsonRes = JsonConvert.SerializeObject(user);
                var content = new StringContent(jsonRes, Encoding.UTF8, "application/json");

                var result = await httpClient.PostAsync(url, content);
                return result.IsSuccessStatusCode;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
         *Http delete
         */
        public async Task<bool> DeleteById(String url)
        {
            try
            {
                HttpResponseMessage response = await httpClient.DeleteAsync(url);
                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*
         * Upload l'image dans le service web
         * renvoie le lien ou chemain
         */
        public async Task<string> UploadImageFile(MultipartFormDataContent content)
        {
            const string url = "http://localhost:61121/api/Uploads/";
         
            try
            {
                var responseMessage = httpClient.PostAsync(url, content).Result;

                if (responseMessage.IsSuccessStatusCode)
                {
                    //return await responseMessage.Content.ReadAsStringAsync();

                    return responseMessage.Content.ReadAsAsync<string>().Result;
                }

                //var e = responseMessage.StatusCode;
                return null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
