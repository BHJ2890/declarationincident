﻿using AdminManager.Models.POCO;
using AdminManager.Models.RestClients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Web;
using AdminManager.Models.Services;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AdminManager.Models.ViewModel
{
    public class IncidentTypeVM
    {
        public int idIncidentType { get; set; }

        [Required]
        [Display(Name = "Type d'incident")]
        public string Libelle { get; set; }

        public string imageTitle { get; set; }
        public byte[] fileContent { get; set; }

        [Display(Name = "Upload file")]
        public string imagePath { get; set; }

        public int idUser { get; set; }
        public virtual User user { get; set; }

        public Byte[] GetFile(string fileName)
        {
            var image = new IncidentTypeServices();
            //return await image.GetFile(fileName);
            var result = image.GetFile(fileName).Result;
            return result;
        }

        public List<IncidentType> element { get; set; }
    }
}