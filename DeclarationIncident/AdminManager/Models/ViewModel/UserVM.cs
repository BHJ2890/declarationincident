﻿using AdminManager.Models.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminManager.Models.ViewModel
{
    public class UserVM
    {
        public int idUser { get; set; }
        [Display(Name = "Prénom "), MaxLength(50)]
        public string firstName { get; set; }

        [Required]
        [Display(Name = "Nom "), MaxLength(30)]
        public string lastName { get; set; }

        [Required]
        [Display(Name = "Mot de passe"), MaxLength(20), MinLength(6), DataType(DataType.Password)]
        public string password { get; set; }

        [Required]
        [Compare("password")]
        [Display(Name = "Confirmation "), MaxLength(20), MinLength(6), DataType(DataType.Password)]
        public string confirmPassword { get; set; }


        [DataType(dataType: DataType.EmailAddress)]
        [Required, Display(Name = "Email"), MaxLength(50)]
        public string email { get; set; }

        [Required]
        public int idRole { get; set; }


        public string Libelle { get; set; }

        public  List<Role> roles { get; set; }

        public  List<Incident> Incident { get; set; }
        public  List<Information> Information { get; set; }
        public  List<IncidentType> IncidentType { get; set; }
        public  List<Notification> Notification { get; set; }
    }
}