﻿using AdminManager.Models.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminManager.Models.ViewModel
{
    public class NotificationVM
    {
       public Incident Incident { get; set; }
       public User user { get; set; }

       public int idNotification { get; set; }
       [Display(Name = "Notification")]
        public string Description { get; set; }
       public int idIncident { get; set; }

    }
}