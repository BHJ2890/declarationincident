﻿using AdminManager.Models.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminManager.Models.ViewModel
{
    public class ToDoTaskVM
    {
        public int IdToDoTask { get; set; }

        public IEnumerable<User> UsersList { get; set; }

        public string incidentDescription { get; set; }
        public string incidentLocation { get; set; }

        [Display(Name = "Detail")]
        public string nameTask { get; set; }
        [Display(Name = "Date")]
        public DateTime dateTask { get; set; }
        [Display(Name = "Libelle incident")]
        public int idIncident { get; set; }
        public int idUser { get; set; }
        [Display(Name = "Tache")]
        public int idTaskState { get; set; }

        public virtual Incident incident { get; set; }
        public virtual List<IncidentStatut> incidentStatut { get; set; }
        public virtual User user { get; set; }

    }
}