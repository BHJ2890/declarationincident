﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminManager.Models.ViewModel
{ 
    public class LoginInfos
    {
        [Required]
        [Display(Name = "Mot de passe"), MaxLength(20), MinLength(6), DataType(DataType.Password)]
        public string password { get; set; }

        [DataType(dataType: DataType.EmailAddress)]
        [Required, Display(Name = "Email"), MaxLength(50)]
        public string email { get; set; }
    }
}