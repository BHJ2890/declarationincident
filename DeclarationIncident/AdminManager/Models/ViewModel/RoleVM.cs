﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using Role = AdminManager.Models.POCO.Role;

namespace AdminManager.Models.ViewModel
{
    public class RoleVM
    {
        public  int idUserCategory { get; set; }
        public string libelle { get; set; }

        public List<Role> categories { get; set; }

    }
}