﻿using AdminManager.Models.POCO;
using AdminManager.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AdminManager.Models.ViewModel
{
    public class IncidentVM
    {
        public int idIncident { get; set; }
        public string Description { get; set; }
        public string imageTitle { get; set; }
        public string imagePath { get; set; }
        public string dateIncident { get; set; }

        public int idLocation { get; set; }
        public int idIncidentType { get; set; }
        public int idUser { get; set; }
        public int IdIncidentState { get; set; }

        public Location Location { get; set; }
        public User user { get; set; }
        public IncidentType incidentType { get; set; }
        public IncidentStatut incidentStatut { get; set; }

        public List<Comment> comment { get; set; }
        public List<Notification> notification { get; set; }

        /*
         * Variable additive
         */
        public string taskstatut { get; set; }

        /*
         * 
         */
        public Byte[] GetFile(string fileName)
        {
            var image = new IncidentTypeServices();
            //return await image.GetFile(fileName);
            var result = image.GetFile(fileName).Result;
            return result;
        }
    }
}