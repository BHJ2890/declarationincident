﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AdminManager.Models.POCO;
using AdminManager.Models.RestClients;

namespace AdminManager.Models.Services
{
    public class NotificationServices
    {
        RestClient<Notification> restClient = new RestClient<Notification>();

        private const string url = "https://localhost:44390/api/Notification/";
        /*
        * Creation d'une nouvelle categorie
        */
        public bool CreateNotification(Notification notification)
        {
            //var result = restClient.PostAsync(notification, url);
            var result = Task.Run(async () => await restClient.PostAsync(notification, url)).Result;
            return result;
        }

        /* var result = Task.Run(async () => await restClient.GetAsync(url)).Result;
         *
         */
        public List<Notification> GetAllNotification()
        {
            var result = Task.Run(async () => await restClient.GetAsync(url)).Result;
            return result;
        }

        /*
         *
         */
        public async Task<Notification> GetEntityById(int id)
        {
            string url = ($"https://localhost:44390/api/Notification/{id}");
            var incidentInfo = await restClient.GetEntityById(url);

            return incidentInfo;
        }

        /*
         *
         */
        public async Task<bool> DeleteIncident(int id)
        {
            string url = ($"https://localhost:44390/api/Notification/{id}");
            var incidentInfo = await restClient.DeleteById(url);

            return incidentInfo;
        }

    }
}