﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AdminManager.Models.POCO;
using AdminManager.Models.RestClients;

namespace AdminManager.Models.Services
{
    public class ToDoTaskStateServices
    {
        RestClient<IncidentStatut> restClient = new RestClient<IncidentStatut>();

        /*
         * Affiche l'id d'une tache
         */
        public async Task<IncidentStatut> GetEntityById(int idIncident)
        {
            string url = ($"https://localhost:44390/api/ToDoTaskStateMethodeProvider?idIncident={idIncident}");
            var incidentInfo = await restClient.GetEntityById(url);

            return incidentInfo;
        }
    }
}