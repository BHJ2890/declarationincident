﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AdminManager.Models.POCO;
using AdminManager.Models.RestClients;

namespace AdminManager.Models.Services
{
    public class ToDoTaskServices
    {
        RestClient<ToDoTask> restClient = new RestClient<ToDoTask>();
        private const string url = "https://localhost:44390/api/ToDoTask/";

        public async Task<bool> CreateToDoTaskServices(ToDoTask toDoTask)
        {
            //Persistence des données
            var result = restClient.PostAsync(toDoTask, url);

            return await result;
        }

    }
}