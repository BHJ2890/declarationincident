﻿using AdminManager.Models.RestClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminManager.Models.POCO;
using System.Threading.Tasks;

namespace AdminManager.Models.Services
{
    public class IncidentServices
    {
        RestClient<Incident> restClients = new RestClient<Incident>();
        private const string url = "https://localhost:44390/api/Incident/";
        /*
       *
       */
        public async Task<List<Incident>> GetIncidentAsync()
        {
           var userList = await restClients.GetAsync(url);
            return userList;
        }

        public async Task<Incident> GetEntityById(int id)
        {
          string url = ($"https://localhost:44390/api/Incident/{id}");
          var incidentInfo = await restClients.GetEntityById(url);

          return incidentInfo;
        }

        public async Task<bool> DeleteIncident(int id)
        {
            string url = ($"https://localhost:44390/api/Incident/{id}");
            var incidentInfo = await restClients.DeleteById(url);

            return incidentInfo;
        }


    }
}