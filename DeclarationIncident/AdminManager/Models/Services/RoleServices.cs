﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AdminManager.Models.POCO;
using AdminManager.Models.RestClients;

namespace AdminManager.Models.Services
{
    public class RoleServices
    {
        RestClient<Role> restClient = new RestClient<Role>();
        private const string url = "https://localhost:44390/api/Role/";
        
        /*
         * Creation d'une nouvelle categorie
         */
        public async Task<bool> CreateCategoryAsinc(Role role)
        {
            //Persistence des données
           var result = restClient.PostAsync(role, url);
           return await result;
        }

        /*
         *
         */
        public async Task<List<Role>> GetAllCategoryAsync()
        {
           var userList = await restClient.GetAsync(url);
           return userList;
        }

        /*
         *
         */
        public async Task<bool> DeleteCategory(int id)
        {
            string url = ($"https://localhost:44390/api/Role/{id}");
            var incidentInfo = await restClient.DeleteById(url);

            return incidentInfo;
        }

        /*
         *
         */
        public async Task<Role> GetEntityById(int id)
        {
            string url = ($"https://localhost:44390/api/Role/{id}");
            var incidentInfo = await restClient.GetEntityById(url);

            return incidentInfo;
        }
    }
}