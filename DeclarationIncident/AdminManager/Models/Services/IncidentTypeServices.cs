﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using AdminManager.Models.CustomAuthentication;
using AdminManager.Models.POCO;
using AdminManager.Models.RestClients;

namespace AdminManager.Models.Services
{
    public class IncidentTypeServices
    {
        private const string url = "https://localhost:44390/api/IncidentType/";
        RestClient<IncidentType> restClients = new RestClient<IncidentType>();
       // CurrentUserInfos currentUserId = new CurrentUserInfos();
        /*
         * 
         */
        public async Task<List<IncidentType>> GetAllIncidentTypeAsync()
        {
            var list_IncidentType = await restClients.GetAsync(url);
            return list_IncidentType;
        }

        /*
         * Creation d'un nouveau type d'incident
         */
        public async Task<bool> CreateIncidentType(IncidentType incidentType, MultipartFormDataContent file)
        {
             var imagePaht = await restClients.UploadImageFile(file);
      
             var entity = new IncidentType()
                {
                    Libelle = incidentType.Libelle,
                    imageTitle = incidentType.imageTitle,
                    imagePath = imagePaht
                };
    
              var result = restClients.PostAsync(entity, url);
              return await result;
        }

        /*
         *
         */
        public async Task<bool> DeleteById(int id)
        {
            var url = ($"https://localhost:44390/api/IncidentType/{id}");
            var result = await restClients.DeleteById(url);

            return result;
        }

        public async Task<Byte[]> GetFile(string fileName)
        {

            var completeUrl = string.Format("https://localhost:44390/api/Uploads/?fileName={0}", fileName);

            using (var httpClients = new HttpClient())
            {
               // var request = string.Format("{0}{1}", baseUrl, fileName);
                //var result = await httpClients.GetAsync(endPoint);

                //return result;
                //return File(result.Content.ReadAsByteArrayAsync().Result,"image/jpeg");
                // return File(outputStream, "application/pdf");
                
                try
                  {
                      

                    httpClients.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("image/jpg"));
                    var response = httpClients.GetAsync(completeUrl).Result;
                    byte[] bytes = response.Content.ReadAsByteArrayAsync().Result;

                    return bytes;

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
    
        }

    }
}