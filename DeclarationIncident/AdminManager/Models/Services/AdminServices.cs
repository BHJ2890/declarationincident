﻿using AdminManager.Models.POCO;
using AdminManager.Models.RestClients;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AdminManager.Models.Services
{
    public class AdminServices
    {
        private const string get_postUrl = "https://localhost:44390/Api/user/";
        RestClient<User> restClient = new RestClient<User>();
        /*
         * methode d'affichage de tout les utilisateur
         */
        public async Task<List<User>> GetAllUserAsync()
        {
            var userList = await restClient.GetAsync(get_postUrl);
            return userList;
        }

        public bool CreateUserAsinc(User user)
        { 
            //Persistence des données
           var result = Task.Run( async () => await restClient.PostAsync(user, get_postUrl)).Result;
           return result;
        }


        public async Task<User> ValidateUser(string email, string password)
        {
            var userList =await restClient.GetAsync(get_postUrl);
            var user = userList.FirstOrDefault(u=>u.email==email && u.password==password);
            //var user = restClient.
           // var user = await restClient.GetAuthenticatedUser(email, password);
            return user;
        }

    }
}