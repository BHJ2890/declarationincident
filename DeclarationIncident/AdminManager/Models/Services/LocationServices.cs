﻿using AdminManager.Models.RestClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AdminManager.Models.POCO;

namespace AdminManager.Models.Services
{
    public class LocationServices
    {
        private const string url = "https://localhost:44390/api/Location/";

        /*
         * Creation d'une nouvelle categorie
         */
        public async Task<bool> CreateLocationAsync(Location userCategory)
        {
            //Persistence des données
            RestClient<Location> restClient = new RestClient<Location>();
            var result = restClient.PostAsync(userCategory, url);

            return await result;
        }

        /*
         *
         */
        public async Task<List<Location>> GetLocationAsync()
        {
            RestClient<Location> restClient = new RestClient<Location>();
            var userList = await restClient.GetAsync(url);

            return userList;
        }
    }
}