﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using AdminManager.Models.POCO;

namespace AdminManager.Models.CustomAuthentication
{
    public class CustomMemberShipUser : MembershipUser
    {
        #region User Properties  

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Role Roles { get; set; }

        #endregion

        public CustomMemberShipUser(User user) : base("CustomMembership", user.firstName, user.idUser, user.email, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            UserId = user.idUser;
            FirstName = user.firstName;
            LastName = user.lastName;
            Roles = user.role;
        }
    }
}