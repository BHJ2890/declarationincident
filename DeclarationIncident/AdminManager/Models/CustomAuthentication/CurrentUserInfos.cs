﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminManager.Models.CustomAuthentication
{
    public class CurrentUserInfos
    {
        public CurrentUserInfos()
        {

        }

        public static int currentUserId()
        {
            return Convert.ToInt32(System.Web.HttpContext.Current.Session["Id"]); 
            
        }
    }
}