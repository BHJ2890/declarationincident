﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class Location
    {
        [Key]
        public int idLocation { get; set; }
        public string nameLocation { get; set; }

        public virtual List<Incident> incident { get; set; }
    }
}