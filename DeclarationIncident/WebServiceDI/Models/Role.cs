﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class Role
    {
        [Key]
        public int idRole { get; set; }
        public string libelle { get; set; }

        public virtual List<User> user { get; set; }
    }
}