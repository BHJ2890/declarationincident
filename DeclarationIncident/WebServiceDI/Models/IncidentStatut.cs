﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class IncidentStatut
    {
        [Key]
        public int IdIncidentStatut { get; set; }
        public string description { get; set; }

        public virtual List<Incident> incident { get; set; }
    }
}