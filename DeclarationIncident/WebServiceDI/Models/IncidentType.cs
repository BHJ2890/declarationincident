﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class IncidentType
    {
        [Key]
        public int idIncidentType { get; set; }
        public string Libelle { get; set; }
        public string imageTitle { get; set; }
        public string imagePath { get; set; }
    }
}