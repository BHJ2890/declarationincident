﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class Comment
    {
        [Key]
        public int idIncidentMention { get; set; }
        public string Libelle { get; set; }
        public int idIncident { get; set; }
        public int idUser { get; set; }

        public virtual Incident incident { get; set; }
        [ForeignKey("idUser")]
        public virtual User user { get; set; }
    }
}