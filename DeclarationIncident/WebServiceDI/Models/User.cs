﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class User
    {
        [Key]
        public int idUser { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public int idRole { get; set; }

        [ForeignKey("idRole")]
        public virtual Role role { get; set; }
        public virtual List<Incident> Incident { get; set; }
        public virtual List<Comment> comment { get; set; }
    }
}