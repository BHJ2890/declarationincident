﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class Information
    {
        [Key]
        public int idInformation { get; set; }
        public string texteInformation { get; set; }
    }
}