﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServiceDI.Models
{
    public class Incident
    {
        [Key]
        public int idIncident { get; set; }
        public string Description { get; set; }
        public string imageTitle { get; set; }
        public string imagePath { get; set; }
        public string dateIncident { get; set; }

        public int idLocation { get; set; }
        public int idIncidentType { get; set; }
        public int idUser { get; set; }
        public int IdIncidentStatut { get; set; }

        public virtual Location Location { get; set; }
        [ForeignKey("idUser")]
        public virtual User user { get; set; }
        public virtual IncidentType incidentType { get; set; }
        public virtual IncidentStatut incidentState { get; set; }

        public virtual List<Comment> comment { get; set; }
        public virtual List<Notification> notification { get; set; }
    }
}