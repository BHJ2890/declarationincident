﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebServiceDI.Models;
using MySql.Data.Entity;


namespace WebServiceDI.DAL
{
    public class dbContext : DbContext
    {
        public dbContext() : base("dbContext")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<WebServiceDI.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.Role> UserCategories { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.Incident> Incidents { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.IncidentType> IncidentTypes { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.Location> Locations { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.Comment> Comment { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.Information> Information { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.Notification> Notifications { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.ToDoTask> ToDoTasks { get; set; }

        public System.Data.Entity.DbSet<WebServiceDI.Models.IncidentStatut> IncidentStates { get; set; }


    }
}
