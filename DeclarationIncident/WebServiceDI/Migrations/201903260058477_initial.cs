namespace WebServiceDI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        idIncidentMention = c.Int(nullable: false, identity: true),
                        Libelle = c.String(unicode: false),
                        idIncident = c.Int(nullable: false),
                        idUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idIncidentMention)
                .ForeignKey("dbo.Incidents", t => t.idIncident, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.idUser, cascadeDelete: true)
                .Index(t => t.idIncident)
                .Index(t => t.idUser);
            
            CreateTable(
                "dbo.Incidents",
                c => new
                    {
                        idIncident = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                        imageTitle = c.String(unicode: false),
                        imagePath = c.String(unicode: false),
                        dateIncident = c.String(unicode: false),
                        idLocation = c.Int(nullable: false),
                        idIncidentType = c.Int(nullable: false),
                        idUser = c.Int(nullable: false),
                        IdIncidentStatut = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idIncident)
                .ForeignKey("dbo.IncidentStatuts", t => t.IdIncidentStatut, cascadeDelete: true)
                .ForeignKey("dbo.IncidentTypes", t => t.idIncidentType, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.idLocation, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.idUser, cascadeDelete: true)
                .Index(t => t.idLocation)
                .Index(t => t.idIncidentType)
                .Index(t => t.idUser)
                .Index(t => t.IdIncidentStatut);
            
            CreateTable(
                "dbo.IncidentStatuts",
                c => new
                    {
                        IdIncidentStatut = c.Int(nullable: false, identity: true),
                        description = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.IdIncidentStatut);
            
            CreateTable(
                "dbo.IncidentTypes",
                c => new
                    {
                        idIncidentType = c.Int(nullable: false, identity: true),
                        Libelle = c.String(unicode: false),
                        imageTitle = c.String(unicode: false),
                        imagePath = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.idIncidentType);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        idLocation = c.Int(nullable: false, identity: true),
                        nameLocation = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.idLocation);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        idNotification = c.Int(nullable: false, identity: true),
                        Description = c.String(unicode: false),
                        idIncident = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idNotification)
                .ForeignKey("dbo.Incidents", t => t.idIncident, cascadeDelete: true)
                .Index(t => t.idIncident);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        idUser = c.Int(nullable: false, identity: true),
                        firstName = c.String(unicode: false),
                        lastName = c.String(unicode: false),
                        password = c.String(unicode: false),
                        email = c.String(unicode: false),
                        idRole = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idUser)
                .ForeignKey("dbo.Roles", t => t.idRole, cascadeDelete: true)
                .Index(t => t.idRole);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        idRole = c.Int(nullable: false, identity: true),
                        libelle = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.idRole);
            
            CreateTable(
                "dbo.Information",
                c => new
                    {
                        idInformation = c.Int(nullable: false, identity: true),
                        texteInformation = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.idInformation);
            
            CreateTable(
                "dbo.ToDoTasks",
                c => new
                    {
                        IdToDoTask = c.Int(nullable: false, identity: true),
                        nameTask = c.String(unicode: false),
                        dateTask = c.DateTime(nullable: false, precision: 0),
                        idIncident = c.Int(nullable: false),
                        idUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdToDoTask)
                .ForeignKey("dbo.Incidents", t => t.idIncident, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.idUser, cascadeDelete: true)
                .Index(t => t.idIncident)
                .Index(t => t.idUser);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ToDoTasks", "idUser", "dbo.Users");
            DropForeignKey("dbo.ToDoTasks", "idIncident", "dbo.Incidents");
            DropForeignKey("dbo.Users", "idRole", "dbo.Roles");
            DropForeignKey("dbo.Incidents", "idUser", "dbo.Users");
            DropForeignKey("dbo.Comments", "idUser", "dbo.Users");
            DropForeignKey("dbo.Notifications", "idIncident", "dbo.Incidents");
            DropForeignKey("dbo.Incidents", "idLocation", "dbo.Locations");
            DropForeignKey("dbo.Incidents", "idIncidentType", "dbo.IncidentTypes");
            DropForeignKey("dbo.Incidents", "IdIncidentStatut", "dbo.IncidentStatuts");
            DropForeignKey("dbo.Comments", "idIncident", "dbo.Incidents");
            DropIndex("dbo.ToDoTasks", new[] { "idUser" });
            DropIndex("dbo.ToDoTasks", new[] { "idIncident" });
            DropIndex("dbo.Users", new[] { "idRole" });
            DropIndex("dbo.Notifications", new[] { "idIncident" });
            DropIndex("dbo.Incidents", new[] { "IdIncidentStatut" });
            DropIndex("dbo.Incidents", new[] { "idUser" });
            DropIndex("dbo.Incidents", new[] { "idIncidentType" });
            DropIndex("dbo.Incidents", new[] { "idLocation" });
            DropIndex("dbo.Comments", new[] { "idUser" });
            DropIndex("dbo.Comments", new[] { "idIncident" });
            DropTable("dbo.ToDoTasks");
            DropTable("dbo.Information");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Notifications");
            DropTable("dbo.Locations");
            DropTable("dbo.IncidentTypes");
            DropTable("dbo.IncidentStatuts");
            DropTable("dbo.Incidents");
            DropTable("dbo.Comments");
        }
    }
}
