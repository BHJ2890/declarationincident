﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceDI.Models;

namespace WebServiceDI.ViewModel
{
    public class RoleVM
    {
        public int idRole { get; set; }
        public string libelle { get; set; }

        //public virtual List<User> user { get; set; }
    }
}