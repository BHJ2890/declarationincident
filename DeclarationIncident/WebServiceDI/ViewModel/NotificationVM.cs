﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceDI.Models;

namespace WebServiceDI.ViewModel
{
    public class NotificationVM
    {
        public int idNotification { get; set; }
        public string Description { get; set; }
        public int idIncident { get; set; }

        public Incident Incident { get; set; }
    }
}