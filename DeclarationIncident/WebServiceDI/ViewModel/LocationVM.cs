﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceDI.Models;

namespace WebServiceDI.ViewModel
{
    public class LocationVM
    {
        public int idLocation { get; set; }
        public string nameLocation { get; set; }

        public List<Incident> incident { get; set; }
    }
}