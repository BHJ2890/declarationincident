﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceDI.Models;

namespace WebServiceDI.ViewModel
{
    public class IncidentTypeVM
    {
        public int idIncidentType { get; set; }
        public string Libelle { get; set; }
        public string imageTitle { get; set; }
        public string imagePath { get; set; }
    }
}