﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServiceDI.Models;

namespace WebServiceDI.ViewModel
{
    public class UserViewModel
    {
        public int idUser { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public int idRole { get; set; }

        public Role role { get; set; }
        public List<Incident> Incident { get; set; }
        public List<Comment> comment { get; set; }
    }
}