﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using WebServiceDI.Security;

namespace WebServiceDI.App_Start
{
    public class BasicAuthenticationUsers : System.Web.Http.Filters.ActionFilterAttribute
    {
        public string BasicRealm { get; set; }
        protected string email { get; set; }
        protected string Password { get; set; }

        public BasicAuthenticationUsers(string email, string password)
        {
            this.email = email;
            this.Password = password;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var req = filterContext.HttpContext.Request;
            var auth = req.Headers["Authorization"];
            if (!String.IsNullOrEmpty(auth))
            {
                var cred = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(auth.Substring(6))).Split(':');
                var email = cred[0];
                var password = cred[1];

                if (UserAuthenticated.Login(email, password))
                {
                   Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(email),null);
                }
                else
                {
                    filterContext.HttpContext.Response.AddHeader("WWW-Authenticate",
                        String.Format("Basic realm=\"{0}\"", BasicRealm ?? "Ryadel"));
                    filterContext.Result = new HttpUnauthorizedResult();
                }
            }

            filterContext.HttpContext.Response.AddHeader("WWW-Authenticate",
                String.Format("Basic realm=\"{0}\"", BasicRealm ?? "Ryadel"));

            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}