﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebServiceDI.DAL;
using WebServiceDI.Models;

namespace WebServiceDI.Controllers
{
    public class IncidentStatutController : ApiController
    {
        private dbContext db = new dbContext();

        // GET: api/IncidentStatut
        public IQueryable<IncidentStatut> GetIncidentStates()
        {
            return db.IncidentStates;
        }

        // GET: api/IncidentStatut/5
        [ResponseType(typeof(IncidentStatut))]
        public IHttpActionResult GetIncidentStatut(int id)
        {
            IncidentStatut incidentStatut = db.IncidentStates.Find(id);
            if (incidentStatut == null)
            {
                return NotFound();
            }

            return Ok(incidentStatut);
        }

        // PUT: api/IncidentStatut/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutIncidentStatut(int id, IncidentStatut incidentStatut)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != incidentStatut.IdIncidentStatut)
            {
                return BadRequest();
            }

            db.Entry(incidentStatut).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IncidentStatutExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/IncidentStatut
        [ResponseType(typeof(IncidentStatut))]
        public IHttpActionResult PostIncidentStatut(IncidentStatut incidentStatut)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.IncidentStates.Add(incidentStatut);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = incidentStatut.IdIncidentStatut }, incidentStatut);
        }

        // DELETE: api/IncidentStatut/5
        [ResponseType(typeof(IncidentStatut))]
        public IHttpActionResult DeleteIncidentStatut(int id)
        {
            IncidentStatut incidentStatut = db.IncidentStates.Find(id);
            if (incidentStatut == null)
            {
                return NotFound();
            }

            db.IncidentStates.Remove(incidentStatut);
            db.SaveChanges();

            return Ok(incidentStatut);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IncidentStatutExists(int id)
        {
            return db.IncidentStates.Count(e => e.IdIncidentStatut == id) > 0;
        }
    }
}