﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebServiceDI.DAL;
using WebServiceDI.Models;

namespace WebServiceDI.Controllers
{
    public class RoleController : ApiController
    {
        private dbContext db = new dbContext();

        // GET: api/Role
        public List<ViewModel.RoleVM> GetRole()
        {
            var listCat = new List<ViewModel.RoleVM>();
            var categoie = db.UserCategories.ToList();

            foreach (var items in categoie)
            {
                listCat.Add(
                    new ViewModel.RoleVM()
                    {
                        libelle = items.libelle,
                        idRole = items.idRole
                      //  user = items.user
                    });
            }
            return listCat;
        }

        // GET: api/Role/5
        [ResponseType(typeof(Role))]
        public IHttpActionResult GetRole(int id)
        {
            Role role = db.UserCategories.Find(id);
            if (role == null)
            {
                return NotFound();
            }

            return Ok(role);
        }

        // PUT: api/Role/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRole(int id, Role role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != role.idRole)
            {
                return BadRequest();
            }

            db.Entry(role).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Role
        [ResponseType(typeof(Role))]
        public IHttpActionResult PostRole(Role role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserCategories.Add(role);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = role.idRole }, role);
        }

        // DELETE: api/Role/5
        [ResponseType(typeof(Role))]
        public IHttpActionResult DeleteRole(int id)
        {
            Role role = db.UserCategories.Find(id);
            if (role == null)
            {
                return NotFound();
            }

            db.UserCategories.Remove(role);
            db.SaveChanges();

            return Ok(role);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleExists(int id)
        {
            return db.UserCategories.Count(e => e.idRole == id) > 0;
        }
    }
}