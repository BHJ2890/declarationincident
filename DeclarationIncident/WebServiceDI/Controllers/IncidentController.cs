﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebServiceDI.DAL;
using WebServiceDI.Models;
using WebServiceDI.ViewModel;

namespace WebServiceDI.Controllers
{
    public class IncidentController : ApiController
    {
        private dbContext db = new dbContext();

        // GET: api/Incident
        public List<IncidentVM> GetIncidents()
        {
            var list = db.Incidents.ToList();
            var listInc = new List<IncidentVM>();

            foreach (var items in list)
            {
                listInc.Add(
                    new IncidentVM()
                    {
                        idIncident = items.idIncident,
                        Description = items.Description,
                        imageTitle = items.imageTitle,
                        imagePath = items.imagePath,
                       
                        dateIncident = items.dateIncident,
                        idIncidentType = items.idIncidentType,
                        idUser = items.idUser,
                        idLocation = items.idLocation,
                        IdIncidentState = items.IdIncidentStatut,
                        user = items.user,
                        Location = items.Location,
                        incidentType = items.incidentType,
                        incidentStatut = items.incidentState,
                        notification = items.notification,
                        comment = items.comment
                    });
            }

            return listInc;
        }

        // GET: api/Incident/5
        [ResponseType(typeof(Incident))]
        public IHttpActionResult GetIncident(int id)
        {
            Incident incident = db.Incidents.Find(id);
            if (incident == null)
            {
                return NotFound();
            }

            return Ok(incident);
        }

        // PUT: api/Incident/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutIncident(int id, Incident incident)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != incident.idIncident)
            {
                return BadRequest();
            }

            db.Entry(incident).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IncidentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Incident
        [ResponseType(typeof(Incident))]
        public IHttpActionResult PostIncident(Incident incident)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Incidents.Add(incident);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = incident.idIncident }, incident);
        }

        // DELETE: api/Incident/5
        [ResponseType(typeof(Incident))]
        public IHttpActionResult DeleteIncident(int id)
        {
            Incident incident = db.Incidents.Find(id);
            if (incident == null)
            {
                return NotFound();
            }

            db.Incidents.Remove(incident);
            db.SaveChanges();

            return Ok(incident);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IncidentExists(int id)
        {
            return db.Incidents.Count(e => e.idIncident == id) > 0;
        }
    }
}