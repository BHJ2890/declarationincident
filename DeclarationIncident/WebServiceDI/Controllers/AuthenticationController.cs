﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServiceDI.App_Start;
using WebServiceDI.DAL;
using WebServiceDI.Models;

namespace WebServiceDI.Controllers
{
    public class AuthenticationController : ApiController
    {
        dbContext db = new dbContext();

        //[BasicAuthenticationUsers("email", "password")]
        [System.Web.Mvc.Route("api/Authentication/?password={password}&email={email}")]
        public User Get(string password, string email)
        {
            var user = db.Users.FirstOrDefault(u => u.email == email && u.password == password);
            return user;
        }
    }
}
