﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using WebServiceDI.DAL;


namespace WebServiceDI.Controllers
{
    public class UploadsController : ApiController
    {
        //Conext
        private dbContext db = new dbContext();

        [System.Web.Mvc.Route("api/Uploads")]
        public string Post()
        {
            try
            {
                var httpResquest = HttpContext.Current.Request;

                if (httpResquest.Files.Count > 0)
                {
                    foreach (string file in httpResquest.Files)
                    {
                        var postedFile = httpResquest.Files[file];
                        var fileName = postedFile.FileName.Split('\\')
                                    .LastOrDefault().Split('/').LastOrDefault();

                        var filePath = HttpContext.Current.Server.MapPath("~/Uploads/" + fileName);
                        postedFile.SaveAs(filePath);

                        //return filePath;
                        //  return "~/Uploads/"+fileName;

                        return fileName;
                    }
                }

                return null;
            }

            catch (Exception e)
            {
                return e.Message;
            }
        }

        /*
         * Obtient une image part son nom
         */
        [System.Web.Mvc.Route("api/Uploads/")]
        public HttpResponseMessage Get(string fileName )
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            var path = "~/Uploads/?fileName=" + fileName;
            var filePath = HostingEnvironment.MapPath(path);

            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            Image image = Image.FromStream(fileStream);
            MemoryStream memoryStream = new MemoryStream();

            image.Save(memoryStream, ImageFormat.Jpeg);
            result.Content = new ByteArrayContent(memoryStream.ToArray());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");

            return result;
        }
    }
}