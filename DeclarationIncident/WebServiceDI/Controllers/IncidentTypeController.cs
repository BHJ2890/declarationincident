﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebServiceDI.DAL;
using WebServiceDI.Models;
using WebServiceDI.ViewModel;

namespace WebServiceDI.Controllers
{
    public class IncidentTypeController : ApiController
    {
        private dbContext db = new dbContext();

        // GET: api/IncidentType
        public List<IncidentTypeVM> GetIncidentTypes()
        {
            var listIncidentType = db.IncidentTypes.ToList();
            var list_IncidentType = new List<IncidentTypeVM>();

            foreach (var items in listIncidentType)
            {
                list_IncidentType.Add(
                    new IncidentTypeVM()
                    {
                        idIncidentType = items.idIncidentType,
                        Libelle = items.Libelle,
                        imageTitle = items.imageTitle,
                        imagePath = items.imagePath,
                    });
            }
            return list_IncidentType;
        }

        // GET: api/IncidentType/5
        [ResponseType(typeof(IncidentType))]
        public IHttpActionResult GetIncidentType(int id)
        {
            IncidentType incidentType = db.IncidentTypes.Find(id);
            if (incidentType == null)
            {
                return NotFound();
            }

            return Ok(incidentType);
        }

        // PUT: api/IncidentType/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutIncidentType(int id, IncidentType incidentType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != incidentType.idIncidentType)
            {
                return BadRequest();
            }

            db.Entry(incidentType).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IncidentTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/IncidentType
        [ResponseType(typeof(IncidentType))]
        public IHttpActionResult PostIncidentType(IncidentType incidentType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.IncidentTypes.Add(incidentType);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = incidentType.idIncidentType }, incidentType);
        }

        // DELETE: api/IncidentType/5
        [ResponseType(typeof(IncidentType))]
        public IHttpActionResult DeleteIncidentType(int id)
        {
            IncidentType incidentType = db.IncidentTypes.Find(id);
            if (incidentType == null)
            {
                return NotFound();
            }

            db.IncidentTypes.Remove(incidentType);
            db.SaveChanges();

            return Ok(incidentType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IncidentTypeExists(int id)
        {
            return db.IncidentTypes.Count(e => e.idIncidentType == id) > 0;
        }
    }
}